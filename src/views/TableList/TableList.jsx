import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { thArray } from "variables/Variables.jsx";
import { Link } from "react-router-dom";

const API = '/api/shipping?';
const DEFAULT_QUERY = '';

class TableList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shippings: [],
    };
  }

  componentDidMount() {
    fetch(API + DEFAULT_QUERY)
      .then(response => response.json())
      .then(data => this.setState({ shippings: data }));
  }

  render() {

    const shippings = this.state.shippings
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Tabelas de Frete Vigentes"
                category="Relação das tabelas de frete por tipo de carga, kilometragem, eixo e carga"
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover>
                    <thead>
                      <tr>
                        {thArray.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {shippings.map(shipping => {
                        return (
                          <tr key={shipping.id}>
                            <td>{shipping.initialDistanceKm}</td>
                            <td>{shipping.finalDistanceKm}</td>
                            <td>R$ {shipping.valueKmAxis}</td>
                            <td>{shipping.loadTypeId}</td>
                            <td>                              
                                <Link  to={{
                                  pathname: "/shipping",
                                  shipping: shipping
                                }}>
                                  <i className="pe-7s-pen" title="Editar variação de frete"/>
                                </Link>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default TableList;
