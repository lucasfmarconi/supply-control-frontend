import React, { Component } from "react";
import { Grid, Row, Col, DropdownButton, MenuItem, FormGroup, ControlLabel } from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

const API = '/api/loadtype?';
const DEFAULT_QUERY = '';

class ShippingForm extends Component {
  static contextTypes = {
    router: () => true, // replace with PropTypes.object if you use them
  }

  constructor(props) {
    super(props);
    
    this.state  = { 
      id: "",
      initialDistanceKm: "",
      finalDistanceKm: "",
      valueKmAxis: "",
      loadTypeId: "",
      loadTypeDesc: "",
      loadTypes: []
    };
    
    const { shipping } = this.props.location;
    if(shipping !== undefined) {
      this.state = { 
        id: shipping.id,
        initialDistanceKm: shipping.initialDistanceKm,
        finalDistanceKm: shipping.finalDistanceKm,
        valueKmAxis: shipping.valueKmAxis,
        loadTypeId: shipping.loadTypeId,
        loadTypeDesc: "",
        loadTypes: []
      };
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleDropdownChange = this.handleDropdownChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    console.log(event);
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleDropdownChange(value) {
    console.log(value);
    console.log(this.state.loadTypes);
    var description = this.setDescriptionLoadType(value);
    this.setState({
      "loadTypeId": value,
      "loadTypeDesc": description
    }); 
  }

  setDescriptionLoadType(loadTypeId) {
    var description = "Selecione";
    this.state.loadTypes.filter(function(l){
      if(l.id === loadTypeId)
      description = l.description;
      return description;
    });
    return description;
  }

  handleSubmit(event) {
    var method = this.state.id > 0 ? "PUT" : "POST";

    (async () => {
      const rawResponse = await fetch('/api/shipping/' + this.state.id, {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.state)
      });
      const content = await rawResponse.json();
      console.log(content);
      // if(method === "POST")
      // {
      //   window.navi("/shipping/" + content.id);
      // }
    })();

    event.preventDefault();
  }

  componentWillMount() {
    
  }

  componentDidMount() {
    fetch(API + DEFAULT_QUERY)
      .then(response => response.json())
      .then(data => this.setState({ loadTypes: data }));
  }

  render() {
    const loadTypes = this.state.loadTypes;
    var title = "Criar Item Tabela de Frete";
    
    if(this.state.id > 0) {
      this.state.loadTypeDesc = this.setDescriptionLoadType(this.state.loadTypeId);
      title = "Editar Item Tabela de Frete";
    }
    
    return (
      <div className="content">
        <Grid fluid>
        <Row>
            <Col md={12}>
              <Card
                title={title}
                content={
                  <form onSubmit={this.handleSubmit}>
                    <FormInputs
                      ncols={["col-md-5"]}
                      proprieties={[
                        {
                          name: "initialDistanceKm",
                          label: "De KM",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "0-3000",
                          value: this.state.initialDistanceKm,
                          onChange: this.handleChange
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-5"]}
                      proprieties={[
                        {
                          name: "finalDistanceKm",
                          label: "Até KM",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "0-3000",
                          value: this.state.finalDistanceKm,
                          onChange: this.handleChange
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-5"]}
                      proprieties={[
                        {
                          name: "valueKmAxis",
                          label: "Preço eixo por KM",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "R$",
                          value: this.state.valueKmAxis,
                          onChange: this.handleChange
                        }
                      ]}
                    />
                    <FormGroup>
                      <ControlLabel>TIPO DE CARGA</ControlLabel>
                      <br />
                      <DropdownButton
                          title={this.state.loadTypeDesc === "" ? "Selecione" : this.state.loadTypeDesc}
                          id="tipodecarga"
                          onSelect={this.handleDropdownChange}
                        >
                          {loadTypes.map(lType => {
                            return (
                              <MenuItem eventKey={lType.id} className={(this.state.loadTypeId === lType.id) ? "active" : ""}>{lType.description}</MenuItem>                              
                            );
                          })}
                      </DropdownButton>
                    </FormGroup>
                    <Col md={2}>
                      <Button
                        bsStyle="default"
                        fill
                        onClick={this.context.router.history.goBack}>
                          Voltar
                      </Button>
                    </Col>
                    <Col md={2}>
                      <Button bsStyle="info" fill type="submit">
                        Salvar 
                      </Button>
                    </Col>                    
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ShippingForm;
