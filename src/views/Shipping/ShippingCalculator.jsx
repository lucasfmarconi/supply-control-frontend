import React, { Component } from "react";
import { Grid, Row, Col, DropdownButton, MenuItem, FormGroup, ControlLabel } from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

const API = '/api/loadtype?';
const DEFAULT_QUERY = '';

class ShippingForm extends Component {
  static contextTypes = {
    router: () => true, // replace with PropTypes.object if you use them
  }

  constructor(props) {
    super(props);
    
    this.state  = {
      distanceKm: "",
      qtdAxis: "",
      loadTypeId: "",
      loadTypeDesc: "",
      shippingCalculated : "",
      shippingCalcVisible : false,
      loadTypes: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleDropdownChange = this.handleDropdownChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    console.log(event);
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleDropdownChange(value) {
    var description = this.setDescriptionLoadType(value);
    this.setState({
      "loadTypeId": value,
      "loadTypeDesc": description
    }); 
  }

  setDescriptionLoadType(loadTypeId) {
    var description = "Selecione";
    this.state.loadTypes.filter(function(l){
      if(l.id === loadTypeId)
      description = l.description;
      return description;
    });
    return description;
  }

  handleSubmit(event) {
    var method = "POST";

    var objToGo = {
      'distance' : this.state.distanceKm,
      'qtdAxis' : this.state.qtdAxis,
      'loadTypeId' : this.state.loadTypeId
    };

    console.log(objToGo);

    (async () => {
      const rawResponse = await fetch('/api/shipping/calculate', {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(objToGo)
      });
      const content = await rawResponse.json();
      console.log(content);
      this.setState({
        shippingCalcVisible : true,
        shippingCalculated : content.toString().replace('.', ',')
      })
    })();

    event.preventDefault();
  }

  componentDidMount() {
    fetch(API + DEFAULT_QUERY)
      .then(response => response.json())
      .then(data => this.setState({ loadTypes: data }));
  }

  render() {
    const loadTypes = this.state.loadTypes;
    var title = "Calcular frete";
    
    return (
      <div className="content">
        <Grid fluid>
        <Row>
            <Col md={12}>
            <div style={{display: !this.state.shippingCalcVisible ? "block" : "none"}}>
              <Card
                title={title}
                content={
                  <form onSubmit={this.handleSubmit}>
                    <FormInputs
                      ncols={["col-md-5"]}
                      proprieties={[
                        {
                          name: "distanceKm",
                          label: "Distância (KMs)",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "0-3000",
                          onChange: this.handleChange
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-5"]}
                      proprieties={[
                        {
                          name: "qtdAxis",
                          label: "Quantidade de eixos",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "0-12",
                          onChange: this.handleChange
                        }
                      ]}
                    />
                    <FormGroup>
                      <ControlLabel>TIPO DE CARGA</ControlLabel>
                      <br />
                      <DropdownButton
                          title={this.state.loadTypeDesc === "" ? "Selecione" : this.state.loadTypeDesc}
                          id="tipodecarga"
                          onSelect={this.handleDropdownChange}
                        >
                          {loadTypes.map(lType => {
                            return (
                              <MenuItem eventKey={lType.id} className={(this.state.loadTypeId === lType.id) ? "active" : ""}>{lType.description}</MenuItem>                              
                            );
                          })}
                      </DropdownButton>
                    </FormGroup>
                    <Col md={2}>
                      <Button bsStyle="info" fill type="submit">
                        Calcular 
                      </Button>
                    </Col>                    
                    <div className="clearfix" />
                  </form>
                }
              />
              </div>
              <div style={{display: this.state.shippingCalcVisible ? "block" : "none"}}>
              <Card 
                title="Frete Calculado"
                content={
                <div className="typo-line">
                  <h4>
                    FRETE MÍNIMO – BASEADO NA TABELA DE FRETES
                    <br/>R$ {this.state.shippingCalculated}<br/>
                    O VALOR ACIMA CORRESPONDE AO TRANSPORTE DE {this.state.loadTypeDesc.toUpperCase()}, COM UM VEÍCULO DE {this.state.qtdAxis} EIXOS, RODANDO {this.state.distanceKm} KM.
                  </h4>
                </div>
                }
                />
                </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ShippingForm;
